﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TwoFactorExt
{
    public partial class SplashScreen : Form
    {
        public SplashScreen(string message)
        {
            InitializeComponent();
            label1.Text += message;
            SendToBack();
        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {

        }

        private void ProgressBar1_Click(object sender, EventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}
