/*
  
*/

using System;
using System.Windows.Forms;

using KeePass.Forms;
using KeePass.Plugins;
using KeePassLib.Utility;


namespace TwoFactor
{
	public sealed class TwoFactorExt : Plugin
	{
		private IPluginHost m_host = null;

        /**
         * Called from keepass to initialize two factor plugin.
         */
        public override bool Initialize(IPluginHost host)
		{
			if(host == null) return false;
			this.m_host = host;

			this.m_host.MainWindow.FileSaved += this.OnFileSaved;           // Notify user when DB is saved.
			return true;
		}

		/**
         *  Called from keepass to tear down two factor plugin.
         */ 
		public override void Terminate()
		{
			// Remove event handler (important!)
			m_host.MainWindow.FileSaved -= this.OnFileSaved;
		}

		/**
         * Add two factor setup to the menu.
         *
         */
        public override ToolStripMenuItem GetMenuItem(PluginMenuType t)
		{
			if(t != PluginMenuType.Main) return null;

			ToolStripMenuItem tsmi = new ToolStripMenuItem("Two Factor Setup...");
            tsmi.Click += this.showSetupMenu;

			return tsmi;
		}

        /**
         * Load the two factor window.
         */ 
        private void showSetupMenu(object sender, EventArgs e)
        {
            TwoFactorForm form = new TwoFactorForm(m_host);
            form.Show();
        }

        /**
         * Save the database.
         */ 
		private void OnFileSaved(object sender, FileSavedEventArgs e)
		{
			MessageService.ShowInfo("SamplePlugin has been notified that the user tried to save to the following file:",
				e.Database.IOConnectionInfo.Path, "Result: " +
				(e.Success ? "success." : "failed."));
		}
	}
}