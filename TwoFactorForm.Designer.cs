﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TwoFactor
{
    partial class TwoFactorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private List<int> myCords = new List<int>() {42, 77, 299, 77, 556, 77, 42, 167, 299, 167, 556, 167, 42, 252, 299, 252, 556, 252};

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
#region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            int index = 0;
            bool services_enabled_flag = false;

            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TwoFactorForm));
            this.setupTwoFactorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.setupTwoFactorLabel.AutoSize = true;
            this.setupTwoFactorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setupTwoFactorLabel.Location = new System.Drawing.Point(12, 20);
            this.setupTwoFactorLabel.Name = "label1";
            this.setupTwoFactorLabel.Size = new System.Drawing.Size(445, 29);
            this.setupTwoFactorLabel.TabIndex = 0;
            this.setupTwoFactorLabel.Text = "Select a website to begin two-factor authentication setup";

            // 
            // MicrosoftButton
            // 
            if (button_enabled("microsoft"))
            {
                this.MicrosoftButton = new System.Windows.Forms.Button();
                this.MicrosoftButton.AccessibleDescription = "Set up two factor for Microsoft.";
                this.MicrosoftButton.AccessibleName = "Microsoft";
                this.MicrosoftButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("MicrosoftButton.BackgroundImage")));
                this.MicrosoftButton.Location = new System.Drawing.Point(myCords[index], myCords[index + 1]);
                this.MicrosoftButton.Name = "MicrosoftButton";
                this.MicrosoftButton.Size = new System.Drawing.Size(224, 68);
                this.MicrosoftButton.TabIndex = 2;
                this.MicrosoftButton.UseVisualStyleBackColor = true;
                this.MicrosoftButton.Click += new System.EventHandler(this.MicrosoftButton_Click);
                this.Controls.Add(this.MicrosoftButton);
                index += 2;
                services_enabled_flag = true;
            }
            // 
            // RedditButton
            // 
            if (button_enabled("reddit"))
            {
                this.RedditButton = new System.Windows.Forms.Button();
                this.RedditButton.AccessibleDescription = "Set up two factor for Reddit.";
                this.RedditButton.AccessibleName = "Reddit";
                this.RedditButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("RedditButton.BackgroundImage")));
                this.RedditButton.Location = new System.Drawing.Point(myCords[index], myCords[index + 1]);
                this.RedditButton.Name = "RedditButton";
                this.RedditButton.Size = new System.Drawing.Size(223, 68);
                this.RedditButton.TabIndex = 3;
                this.RedditButton.UseVisualStyleBackColor = true;
                this.RedditButton.Click += new System.EventHandler(this.RedditButton_Click);
                this.Controls.Add(this.RedditButton);
                index += 2;
                services_enabled_flag = true;
            }
            // 
            // GoogleButton
            // 
            if (button_enabled("google"))
            {
                this.GoogleButton = new System.Windows.Forms.Button();
                this.GoogleButton.AccessibleDescription = "Set up two factor for Google.";
                this.GoogleButton.AccessibleName = "Google";
                this.GoogleButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("GoogleButton.BackgroundImage")));
                this.GoogleButton.Location = new System.Drawing.Point(myCords[index], myCords[index + 1]);
                this.GoogleButton.Name = "GoogleButton";
                this.GoogleButton.Size = new System.Drawing.Size(224, 68);
                this.GoogleButton.TabIndex = 4;
                this.GoogleButton.UseVisualStyleBackColor = true;
                this.GoogleButton.Click += new System.EventHandler(this.GoogleButton_Click);
                this.Controls.Add(this.GoogleButton);
                index += 2;
                services_enabled_flag = true;
            }
            // 
            // PinterestButton
            // 
            if (button_enabled("pinterest"))
            {
                this.PinterestButton = new System.Windows.Forms.Button();
                this.PinterestButton.AccessibleDescription = "Set up two factor for pinterest.";
                this.PinterestButton.AccessibleName = "Pinterest";
                this.PinterestButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("PinterestButton.BackgroundImage")));
                this.PinterestButton.Location = new System.Drawing.Point(myCords[index], myCords[index + 1]);
                this.PinterestButton.Name = "PinterestButton";
                this.PinterestButton.Size = new System.Drawing.Size(224, 68);
                this.PinterestButton.TabIndex = 5;
                this.PinterestButton.UseVisualStyleBackColor = true;
                this.PinterestButton.Click += new System.EventHandler(this.PinterestButton_Click);
                this.Controls.Add(this.PinterestButton);
                index += 2;
                services_enabled_flag = true;
            }
            // 
            // AmazonButton
            // 
            if (button_enabled("amazon"))
            {
                this.AmazonButton = new System.Windows.Forms.Button();
                this.AmazonButton.AccessibleDescription = "Set up two factor for Amazon.";
                this.AmazonButton.AccessibleName = "Amazon";
                this.AmazonButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("AmazonButton.BackgroundImage")));
                this.AmazonButton.Location = new System.Drawing.Point(myCords[index], myCords[index + 1]);
                this.AmazonButton.Name = "AmazonButton";
                this.AmazonButton.Size = new System.Drawing.Size(224, 69);
                this.AmazonButton.TabIndex = 6;
                this.AmazonButton.UseVisualStyleBackColor = true;
                this.AmazonButton.Click += new System.EventHandler(this.AmazonButton_Click);
                this.Controls.Add(this.AmazonButton);
                index += 2;
                services_enabled_flag = true;
            }
            // 
            // GithubButton
            // 
            if (button_enabled("github"))
            {
                this.GithubButton = new System.Windows.Forms.Button();
                this.GithubButton.AccessibleDescription = "Set up two factor for github.";
                this.GithubButton.AccessibleName = "Github";
                this.GithubButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("GithubButton.BackgroundImage")));
                this.GithubButton.Location = new System.Drawing.Point(myCords[index], myCords[index + 1]);
                this.GithubButton.Name = "GithubButton";
                this.GithubButton.Size = new System.Drawing.Size(224, 69);
                this.GithubButton.TabIndex = 7;
                this.GithubButton.UseVisualStyleBackColor = true;
                this.GithubButton.Click += new System.EventHandler(this.GithubButton_Click);
                this.Controls.Add(this.GithubButton);
                index += 2;
                services_enabled_flag = true;
            }
            // 
            // NetflixButton
            // 
            if (button_enabled("netflix"))
            {
                this.NetflixButton = new System.Windows.Forms.Button();
                this.NetflixButton.AccessibleDescription = "Set up two factor for netflix.";
                this.NetflixButton.AccessibleName = "netflix";
                this.NetflixButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("NetflixButton.BackgroundImage")));
                this.NetflixButton.Location = new System.Drawing.Point(myCords[index], myCords[index + 1]);
                this.NetflixButton.Name = "NetflixButton";
                this.NetflixButton.Size = new System.Drawing.Size(224, 69);
                this.NetflixButton.TabIndex = 8;
                this.NetflixButton.UseVisualStyleBackColor = true;
                this.NetflixButton.Click += new System.EventHandler(this.NetflixButton_Click);
                this.Controls.Add(this.NetflixButton);
                index += 2;
                services_enabled_flag = true;
            }
            // 
            // LinkedInButton
            // 
            if (button_enabled("linkedin"))
            {
                this.LinkedInButton = new System.Windows.Forms.Button();
                this.LinkedInButton.AccessibleDescription = "Set up two factor for linked in.";
                this.LinkedInButton.AccessibleName = "Linked in";
                this.LinkedInButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LinkedInButton.BackgroundImage")));
                this.LinkedInButton.Location = new System.Drawing.Point(myCords[index], myCords[index + 1]);
                this.LinkedInButton.Name = "LinkedInButton";
                this.LinkedInButton.Size = new System.Drawing.Size(224, 69);
                this.LinkedInButton.TabIndex = 9;
                this.LinkedInButton.UseVisualStyleBackColor = true;
                this.LinkedInButton.Click += new System.EventHandler(this.LinkedInButton_Click);
                this.Controls.Add(this.LinkedInButton);
                index += 2;
                services_enabled_flag = true;
            }

            if (services_enabled_flag == false)
            {
                this.noServicesToSetup = new System.Windows.Forms.Label();
                this.noServicesToSetup.AutoSize = true;
                this.noServicesToSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.noServicesToSetup.Location = new System.Drawing.Point(25, 155);
                this.noServicesToSetup.Name = "no2fa";
                this.noServicesToSetup.Size = new System.Drawing.Size(445, 29);
                this.noServicesToSetup.TabIndex = 0;
                this.noServicesToSetup.Text = "Nothing to setup 2FA on!";
                this.Controls.Add(this.noServicesToSetup);

                this.noServicesToSetupDescription = new System.Windows.Forms.Label();
                this.noServicesToSetupDescription.AutoSize = true;
                this.noServicesToSetupDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                this.noServicesToSetupDescription.Location = new System.Drawing.Point(25, 190);
                this.noServicesToSetupDescription.Name = "no2fadescription";
                this.noServicesToSetupDescription.Size = new System.Drawing.Size(445, 29);
                this.noServicesToSetupDescription.TabIndex = 0;
                this.noServicesToSetupDescription.Text = "To setup 2FA add a supported service to the keepass database. If the entry\n is already in the database and is not showing 2FA is already enabled.\n\nSupported Services are: " + String.Join(", ", services_enabled.ToArray());
                this.Controls.Add(this.noServicesToSetupDescription);
            }

            // 
            // TwitterButton
            // 
            if (button_enabled("twitter"))
            {
                this.TwitterButton = new System.Windows.Forms.Button();
                this.TwitterButton.AccessibleDescription = "Set up two factor for twitter.";
                this.TwitterButton.AccessibleName = "Twitter";
                this.TwitterButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("TwitterButton.BackgroundImage")));
                this.TwitterButton.Location = new System.Drawing.Point(myCords[index], myCords[index + 1]);
                this.TwitterButton.Name = "TwitterButton";
                this.TwitterButton.Size = new System.Drawing.Size(224, 69);
                this.TwitterButton.TabIndex = 9;
                this.TwitterButton.UseVisualStyleBackColor = true;
                this.TwitterButton.Click += new System.EventHandler(this.TwitterButton_Click);
                this.Controls.Add(this.TwitterButton);
                index += 2;
                services_enabled_flag = true;
            }

            // 
            // Form1
            // 
            this.AccessibleDescription = "Menu for setting up two factor authentication.";
            this.AccessibleName = "Two Factor Setup";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 472);
            
            this.Controls.Add(this.setupTwoFactorLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("Resources/favicon.ico")));
            this.Name = "Form1";
            this.Text = "Two Factor Setup";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion
        

        private System.Windows.Forms.Label setupTwoFactorLabel;
        private System.Windows.Forms.Label noServicesToSetup;
        private System.Windows.Forms.Label noServicesToSetupDescription;
        private System.Windows.Forms.Button FacebookButton;
        private System.Windows.Forms.Button MicrosoftButton;
        private System.Windows.Forms.Button RedditButton;
        private System.Windows.Forms.Button GoogleButton;
        private System.Windows.Forms.Button PinterestButton;
        private System.Windows.Forms.Button AmazonButton;
        private System.Windows.Forms.Button GithubButton;
        private System.Windows.Forms.Button NetflixButton;
        private System.Windows.Forms.Button LinkedInButton;
        private System.Windows.Forms.Button TwitterButton;
    }
}