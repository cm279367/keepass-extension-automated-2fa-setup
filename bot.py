from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from PIL import Image

import os
import qrcode
import json
import time
import base64
import random
import sys
import urllib.request

'''
    Great article on understanding xpaths for selenium
    https://www.guru99.com/xpath-selenium.html
'''

# Extra code is to block notification pop ups. For example, "Facebook wants to send you notifications ALLOW BLOCK"
chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications": 2}
chrome_options.add_argument('--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36"')
chrome_options.add_argument('log-level=3')
chrome_options.add_experimental_option("prefs", prefs)
# chrome_options.add_argument("--headless")
driver = webdriver.Chrome('./chromedriver.exe', options=chrome_options)  # Optional argument, if not specified will search path.

'''
    Github
'''


def github(username, password, phone_number, auth_type):
    try:
        driver.get('https://github.com/login')
        username_box = driver.find_element_by_name('login')
        time.sleep(random.uniform(0.50, 1.5))
        username_box.send_keys(username)
        time.sleep(random.uniform(0.50, 1.5))
        password_box = driver.find_element_by_name('password')
        time.sleep(random.uniform(0.50, 1.5))
        password_box.send_keys(password)
        time.sleep(random.uniform(0.50, 1.5))
        submit_button = driver.find_element_by_name('commit')
        time.sleep(random.uniform(0.50, 1.5))
        submit_button.submit()
        time.sleep(random.uniform(0.50, 1.5))
        driver.get('https://github.com/settings/two_factor_authentication/intro')

        time.sleep(random.uniform(0.50, 1.5))
        if auth_type != "sms":
            driver.find_element_by_xpath(".//button[text()='Set up using an app']").click()
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(@data-ga-click, 'download')]").click()
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(text(), 'Next')]").click()
            time.sleep(random.uniform(3.0, 4.5))
            element = driver.find_elements_by_xpath('.//details-dialog[@aria-label="Your two-factor secret"]//parent::details-dialog//child::div')[1]
            secret = element.get_attribute('innerText')

            time.sleep(random.uniform(0.50, 1.5))

            # Take screenshot of github auth URL.
            element = driver.find_element_by_xpath('.//table[@class="qr-code-table"]')
            location = element.location
            size = element.size
            png = driver.get_screenshot_as_png()  # saves screenshot of entire page

            # uses PIL library to open image in memory
            from io import BytesIO
            im = Image.open(BytesIO(png))

            left = location['x'] + 5
            top = location['y'] + 5
            right = location['x'] + size['width'] - 5
            bottom = location['y'] + size['height'] - 5

            im = im.crop((left, top, right, bottom))  # defines crop points
            im.save('./qr_temp.png')

            with open('./qr_temp.png', "rb") as f:
                qr_img = f.read()
            os.remove('./qr_temp.png')

            my_json = json.dumps([{'Key': 'QrImage', 'Value': json.dumps(
                {'Image': base64.b64encode(qr_img).decode('ascii'), 'TextCode': secret,
                 "Label": "Enter code from 2FA app."})}])
            print(my_json)
            opt = input()

            code_box = driver.find_element_by_xpath('.//input[@id="two-factor-code"]')
            code_box.send_keys(opt)

            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath('.//button[contains(text(),"Enable")]').click()

            print(json.dumps([{'Key': 'End',
                               'Value': '2FA has been set up. Backup codes downloaded to ~/Downloads folder.'}]))
            end = input()
        else:
            driver.find_element_by_xpath(".//button[text()='Set up using SMS']").click()
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(@data-ga-click, 'download')]").click()
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(text(), 'Next')]").click()
            time.sleep(random.uniform(0.25, 1.5))
            phone_box = driver.find_element_by_xpath(".//input[@name='number']")
            phone_box.send_keys(phone_number)
            time.sleep(random.uniform(0.25, 1.5))
            driver.find_element_by_xpath(".//button[contains(text(), 'Send authentication code')]").click()

            # TODO: Make more automatic with extension.
            time.sleep(random.uniform(0.50, 1.5))
            my_json = json.dumps([{'Key': 'TextData', 'Value': "Enter six digit code."}])
            print(my_json)
            code = input()

            time.sleep(random.uniform(0.50, 1.5))
            code_box = driver.find_element_by_xpath('.//input[@id="two-factor-code"]')
            code_box.send_keys(code)

            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath('.//button[contains(text(),"Enable")]').click()

            print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for ' + phone_number + '. Backup codes downloaded to ~/Downloads folder.'}]))
            end = input()
            return 0
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


'''
   Google
'''


def google(username, password, phone_number, auth_type):
    try:
        time.sleep(random.uniform(0.50, 1.5))
        driver.get('https://myaccount.google.com/security')

        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath(".//a[@aria-label='Sign in']").click()
        time.sleep(random.uniform(0.50, 1.5))
        user_box = driver.find_element_by_xpath(".//input[@aria-label='Email or phone']")
        time.sleep(random.uniform(0.50, 1.5))
        user_box.send_keys(username)
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_id('identifierNext').click()

        time.sleep(random.uniform(0.50, 1.5))
        pass_box = driver.find_element_by_xpath(".//input[@aria-label='Enter your password']")
        time.sleep(random.uniform(0.50, 1.5))
        pass_box.send_keys(password)
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_id('passwordNext').click()

        time.sleep(random.uniform(3.0, 4.5))
        driver.find_element_by_xpath(".//a[contains(@href, 'two-step-verification')]").click()
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath(".//span[text()='Get started']//parent::span//parent::div").click()

        # TODO may need to add a turn on button here if they have already enabled two factor.

        time.sleep(random.uniform(0.50, 1.5))
        pass_box = driver.find_element_by_xpath(".//input[@aria-label='Enter your password']")
        time.sleep(random.uniform(0.55, 1.5))
        pass_box.send_keys(password)
        time.sleep(random.uniform(0.55, 1.5))
        driver.find_element_by_id('passwordNext').click()

        time.sleep(random.uniform(1.0, 1.5))
        pass_box = driver.find_element_by_xpath(".//input[@aria-label='Enter a phone number']")
        time.sleep(random.uniform(1.0, 1.5))
        pass_box.send_keys(phone_number)

        time.sleep(random.uniform(0.50, 1.5))
        if auth_type == "sms":
            driver.find_element_by_xpath(".//span[text()='Text message']//parent::label//child::div").click()
        else:
            driver.find_element_by_xpath(".//span[text()='Phone call']//parent::label//child::div").click()

        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath(".//span[text()='Next']//parent::span//parent::div").click()

        time.sleep(random.uniform(0.50, 1.5))
        my_json = json.dumps([{'Key': 'TextData', 'Value': "Enter OTP"}])
        print(my_json)
        code = input()
        code_box = driver.find_element_by_xpath('.//input[@aria-label="Enter the code"]')
        time.sleep(random.uniform(0.50, 1.5))
        code_box.send_keys(code)

        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath('.//span[text()="Next"]//parent::span//parent::div').click()

        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath('.//span[text()="Turn on"]//parent::span//parent::div').click()

        time.sleep(random.uniform(0.50, 1.5))
        print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for Google.'}]))
        end = input()
        return 0
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


'''
    Pinterest
'''


def pinterest(username, password, phone_number, auth_type):
    try:
        time.sleep(random.uniform(1.5, 2.5))
        driver.get('https://www.pinterest.com/login/')
        time.sleep(random.uniform(1.5, 2.5))
        username_box = driver.find_element_by_name('id')
        time.sleep(random.uniform(1.5, 2.5))
        username_box.send_keys(username)
        time.sleep(random.uniform(1.5, 2.5))
        password_box = driver.find_element_by_name('password')
        time.sleep(random.uniform(1.5, 2.5))
        password_box.send_keys(password)
        time.sleep(random.uniform(1.5, 2.5))
        driver.find_element_by_class_name('SignupButton').click()
        time.sleep(random.uniform(1.5, 2.5))
        driver.get('https://www.pinterest.com/settings/security/')
        time.sleep(random.uniform(2.0, 3.5))
        driver.find_element_by_name('mfa_preference').click()
        time.sleep(random.uniform(0.50, 2.5))
        password_box = driver.find_element_by_id('password')
        password_box.send_keys(password)
        time.sleep(random.uniform(0.50, 2.5))
        driver.find_element_by_xpath("//div[contains(text(), 'Next')]").click()
        driver.implicitly_wait(10)
        number = driver.find_element_by_id('phoneNumber')
        number.clear()
        number.send_keys(phone_number)
        time.sleep(random.uniform(0.50, 2.5))
        driver.find_element_by_xpath("//div[contains(text(), 'Next')]").click()

        driver.switch_to_active_element
        my_json = json.dumps([{'Key': 'TextData', 'Value': "Enter verification code."}])
        print(my_json)
        code = input()

        time.sleep(random.uniform(1.5, 2.5))
        code_box = driver.find_element_by_xpath('.//input[@id="code"]')
        code_box.send_keys(code)

        time.sleep(random.uniform(1.5, 2.5))
        driver.find_element_by_xpath('.//div[contains(text(), "Verify")]//parent::button').click()

        import re

        pattern = re.compile(r"[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]")

        value = "newcode"
        while value == "newcode":
            time.sleep(random.uniform(1.0, 1.5))
            elements = driver.find_elements_by_css_selector("div")
            for element in elements:
                try:
                    # print(element.text)
                    match = pattern.match(element.text)
                    if match:
                        backup_code = re.findall(pattern, element.text)
                        break
                except StaleElementReferenceException:
                    pass

            print(json.dumps([{'Key': 'BackupCode', 'Value': backup_code[0]}]))
            value = input()
            if value == "newcode":
                driver.find_element_by_xpath('.//div[contains(text(), "Get a new code")]//parent::button').click()
            else:
                driver.find_element_by_xpath('.//div[contains(text(), "Done")]//parent::button').click()
                break

        print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for Pinterest.'}]))
        end = input()
        return 0
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1

'''
    Reddit
'''


def reddit(username, password, phone_number, auth_type):
    try:
        driver.get('https://www.reddit.com/login/')
        time.sleep(random.uniform(0.50, 1.5))
        username_box = driver.find_element_by_name('username')
        username_box.send_keys(username)
        time.sleep(random.uniform(0.50, 1.5))
        password_box = driver.find_element_by_name('password')
        password_box.send_keys(password)
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_class_name('AnimatedForm__submitButton').click()
        time.sleep(1)  # wait for authentication before redirect
        driver.get('https://www.reddit.com/prefs/update')
        driver.find_element_by_class_name('open-enable-tfa-popup').click()
        driver.switch_to_active_element

        # Somehow there are multiple elements for each element so we manually select which one we want.
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_elements_by_xpath(".//button[text()='my email is correct']")[1].click()
        time.sleep(random.uniform(0.50, 1.5))
        pass_box = driver.find_elements_by_xpath(".//div[@class='enable-tfa-inputs']//input[@type='password']")[1]
        time.sleep(random.uniform(0.50, 1.5))
        pass_box.send_keys(password)
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_elements_by_xpath(".//button[text()='next']")[2].click()

        time.sleep(random.uniform(1.50, 4.5))
        driver.switch_to_active_element
        canvas = driver.find_element_by_xpath(".//p[@class='secret']//parent::div//canvas")
        png = canvas_to_png(canvas)
        secret = driver.find_element_by_xpath(".//p[@class='secret']").get_attribute('innerText')

        my_json = json.dumps([{"Key": "QrImage", "Value": json.dumps({
            "Image": base64.b64encode(png).decode('ascii'), "TextCode": secret, "Label": "Enter code from 2FA app."})}])
        print(my_json)
        opt = input()

        opt_box = driver.find_elements_by_xpath(".//input[@id='otpfield']")[1]
        opt_box.send_keys(opt)
        driver.find_elements_by_xpath(".//button[text()='enable two-factor']")[1].click()

        time.sleep(random.uniform(0.50, 1.5))
        print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for Reddit.'}]))
        end = input()
        return 0
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


'''
    Facebook
'''


def facebook(username, password, phone_number, auth_type):
    try:
        time.sleep(random.uniform(0.25, 1.5))
        driver.get('https://www.facebook.com/login')
        time.sleep(random.uniform(0.50, 1.5))
        username_box = driver.find_element_by_name('email')
        time.sleep(random.uniform(0.25, 1.5))
        username_box.send_keys(username)
        time.sleep(random.uniform(0.50, 1.5))
        password_box = driver.find_element_by_name('pass')
        time.sleep(random.uniform(0.25, 1.5))
        password_box.send_keys(password)
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_id('loginbutton').click()

        time.sleep(random.uniform(0.25, 1.5))
        driver.get('https://www.facebook.com/security/2fac/setup/intro/')
        time.sleep(random.uniform(3.0, 4.5))
        driver.find_element_by_link_text("Get Started").click()

        time.sleep(random.uniform(1.5, 2.5))
        if auth_type == "sms":
            driver.find_element_by_xpath(".//div[@data-testid='tfa_choose_sms']").click()
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath(".//button[@data-testid='tfa_setup_dialog_primary_button']").click()
            time.sleep(random.uniform(0.50, 1.5))
            driver.switch_to_active_element
            phone_box = driver.find_element_by_xpath(".//input[@data-testid='tfs_add_phone_phone_input']")
            phone_box.send_keys(phone_number)
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath(".//button[@data-testid='tfa_setup_dialog_primary_button']").click()

            try:
                pass_box = driver.find_element_by_xpath(".//input[@data-testid='reauth_password_field']")
                pass_box.send_keys(password)
                driver.find_element_by_xpath(".//button[@data-testid='sec_ac_button']").click()
            except NoSuchElementException:
                pass

            my_json = json.dumps([{'Key': 'TextData', 'Value': "Enter 6 digit code we sent to your phone."}])
            print(my_json)
            code = input()
            digits = list(code)
            for i in range(6):
                digit = str(digits[i]).strip()
                box = driver.find_element_by_xpath('.//input[@data-key="' + str(i) + '"]')
                box.send_keys(digit)
                time.sleep(random.uniform(1.00, 2.0))

            driver.find_element_by_xpath('.//div[contains(text(), "Next")]//parent::div//parent::button').click()

            # TODO: Make fully automated.
        else:
            driver.find_element_by_xpath(".//div[@data-testid='tfa_choose_qr']").click()
            driver.switch_to_active_element
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath(".//button[@data-testid='tfa_setup_dialog_primary_button']").click()
            time.sleep(random.uniform(0.50, 1.5))

            # This option does not work on facebook.com.
            # driver.find_element_by_xpath(".//button[@data-testid='tfa_setup_dialog_primary_button']").click()

    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


def amazon(username, password, phone_number, auth_method):
    try:
        driver.get('http://www.amazon.com/account')
        time.sleep(random.uniform(0.50, 1.5))
        email_box = driver.find_element_by_id("ap_email")
        time.sleep(random.uniform(0.50, 1.5))
        email_box.send_keys(username)
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_id("continue").click()
        time.sleep(random.uniform(0.50, 1.5))
        pass_box = driver.find_element_by_id("ap_password")
        time.sleep(random.uniform(0.50, 1.5))
        pass_box.send_keys(password)
        driver.find_element_by_xpath(".//input[@aria-labelledby='auth-signin-button-announce']").click()

        '''
            !!! CAPTCHA BOX !!!
            - Handles entering incorrect codes and refreshing CAPTCHA's
        '''
        try:
            flag = True
            while flag:
                time.sleep(random.uniform(0.50, 1.5))
                pass_box = driver.find_element_by_id("ap_password")
                pass_box.clear()
                pass_box.send_keys(password)
                captcha_url = driver.find_element_by_xpath(".//img[contains(@alt, 'CAPTCHA')]").get_attribute('src')
                urllib.request.urlretrieve(captcha_url, "./CAPTCHA.png")
                with open("./CAPTCHA.png", "rb") as f:
                    captcha_img = f.read()

                captcha_box = driver.find_element_by_id("auth-captcha-guess")
                captcha = 'n'
                while captcha == "n":
                    my_json = json.dumps([{"Key": "Captcha", "Value": json.dumps(
                        {"Image": base64.b64encode(captcha_img).decode('ascii'), "TextCode": "",
                         "Label": "Enter CAPTCHA"})}])
                    print(my_json)
                    captcha = input()
                    if captcha == "n":
                        driver.find_element_by_id("auth-captcha-refresh-link").click()
                        time.sleep(random.uniform(0.50, 1.5))
                        captcha_url = driver.find_element_by_xpath(".//img[contains(@alt, 'CAPTCHA')]").get_attribute('src')
                        urllib.request.urlretrieve(captcha_url, "./CAPTCHA.png")
                        with open("./CAPTCHA.png", "rb") as f:
                            captcha_img = f.read()
                os.remove("./CAPTCHA.png")
                captcha_box.send_keys(captcha)
                driver.find_element_by_id("signInSubmit").click()

                try:
                    time.sleep(random.uniform(0.50, 1.5))
                    pass_box = driver.find_element_by_id("ap_password")
                    pass_box.send_keys(password)
                    driver.find_element_by_id("signInSubmit").click()
                except NoSuchElementException:
                    flag = False

        except NoSuchElementException:
            pass

        try:
            # Anti Automation Challenge.
            trash = driver.find_element_by_xpath('.//span[text()="Anti-Automation Challenge"]')

            captcha_url = driver.find_element_by_xpath(".//img[contains(@alt, 'captcha')]").get_attribute('src')
            urllib.request.urlretrieve(captcha_url, "./CAPTCHA.png")
            with open("./CAPTCHA.png", "rb") as f:
                captcha_img = f.read()
            os.remove("./CAPTCHA.png")

            captcha_box = driver.find_element_by_xpath(".//input[@type='text']")
            captcha = 'n'
            while captcha == "n":
                my_json = json.dumps([{"Key": "Captcha", "Value": json.dumps({"Image": base64.b64encode(captcha_img).decode('ascii'), "TextCode": "", "Label": "Enter CAPTCHA"})}])
                print(my_json)
                captcha = input()
                if captcha == "n":
                    driver.find_element_by_id(".//a[@data-value='newCaptcha']").click()
                    time.sleep(random.uniform(0.50, 1.5))
                    captcha_url = driver.find_element_by_xpath(".//img[contains(@alt, 'captcha')]").get_attribute('src')
                    urllib.request.urlretrieve(captcha_url, "./CAPTCHA.png")
                    with open("./CAPTCHA.png", "rb") as f:
                        captcha_img = f.read()
                    os.remove("./CAPTCHA.png")
            captcha_box.send_keys(captcha)
            driver.find_element_by_id("signInSubmit").click()

        except NoSuchElementException:
            pass

        # OTP passwords
        try:
            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_id("continue").click()
            otp_box = driver.find_element_by_xpath(".//div[text()='Enter OTP']//parent::div//child::input")
            info_box = driver.find_element_by_xpath(".//span[contains(text(), 'One Time Password (OTP) sent to')]")
            my_json = json.dumps([{"Key": "TextData", "Value": info_box.get_attribute('innerText').strip()}])

            print(my_json)
            opt = input()
            otp_box.send_keys(opt)
            driver.find_element_by_xpath(".//span[text()='Continue']//parent::span//child::input").click()
        except NoSuchElementException:
            pass

        time.sleep(random.uniform(0.50, 1.5))
        driver.get('https://www.amazon.com/gp/css/homepage.html')
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_xpath(".//div[@data-card-identifier='SignInAndSecurity']").click()
        time.sleep(random.uniform(0.50, 1.5))
        driver.find_element_by_id("auth-cnep-advanced-security-settings-button").click()
        time.sleep(random.uniform(0.50, 1.5))
        for element in driver.find_elements_by_xpath(".//a[text()='Remove']"):
            element.click()
            time.sleep(random.uniform(0.50, 1.5))
        driver.find_elements_by_xpath(".//a[text()='Get Started']")[1].click()
        time.sleep(random.uniform(0.50, 1.5))

        if auth_method == 'sms':
            driver.find_element_by_xpath(".//span[contains(text(), 'Phone number')]//parent::h5//parent::a").click()
            time.sleep(random.uniform(0.50, 1.5))

            # Amazon switchs ID's on you to avoid scrapping!
            try:
                phone_box = driver.find_element_by_xpath(".//input[@name='phoneNumber']")
            except NoSuchElementException:
                phone_box = driver.find_element_by_xpath(".//input[@name='cvf_phone_num']")
            time.sleep(random.uniform(0.50, 1.5))
            phone_box.send_keys(phone_number)
            time.sleep(random.uniform(0.50, 1.5))
            try:
                driver.find_element_by_xpath(".//input[@id='add-phone-form-submit']").click()
            except NoSuchElementException:
                driver.find_element_by_xpath(".//span[text()='Continue']//parent::span//child::input").click()

            time.sleep(random.uniform(0.50, 1.5))
            try:
                code_box = driver.find_element_by_xpath(".//input[@name='code']")
            except NoSuchElementException:
                code_box = driver.find_element_by_xpath(".//input[@name='code']")
            my_json = json.dumps([{'Key': 'TextData', 'Value': "Enter OTP"}])
            print(my_json)
            code = input()
            code_box.send_keys(code)
            try:
                time.sleep(random.uniform(0.50, 1.5))
                driver.find_element_by_xpath(".//input[@id='sia-verify-phone-submit']").click()
            except NoSuchElementException:
                driver.find_element_by_xpath(".//input[@name='cvf_action']").click()

            time.sleep(random.uniform(0.50, 1.5))
            driver.find_element_by_xpath(".//input[@id='enable-mfa-form-submit']").click()

            print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for amazon.'}]))
            end = input()
        else:
            # TODO: click authenticator app.
            driver.find_element_by_xpath(".//span[contains(text(), 'Authenticator App')]//parent::h5//parent::a").click()
            time.sleep(random.uniform(0.50, 1.5))
            secret_code = driver.find_element_by_xpath(".//span[@id='sia-auth-app-formatted-secret']").get_attribute('innerText')
            time.sleep(random.uniform(0.50, 1.5))
            barcode_url = driver.find_element_by_xpath(".//img[contains(@src, 'data:image/png')]").get_attribute('src')
            time.sleep(random.uniform(0.50, 1.5))
            urllib.request.urlretrieve(barcode_url, "./barcode.png")
            time.sleep(random.uniform(0.50, 1.5))
            with open("./barcode.png", "rb") as f:
                qr_img = f.read()
                time.sleep(random.uniform(0.50, 1.5))
                my_json = json.dumps([{'Key': 'QrImage', 'Value': json.dumps({'Image': base64.b64encode(qr_img).decode('ascii'), 'TextCode': secret_code, "Label": "Enter code from 2FA app."})}])
                print(my_json)
                time.sleep(random.uniform(0.50, 1.5))
                opt = input()
                time.sleep(random.uniform(0.50, 1.5))
                otp_box = driver.find_element_by_xpath(".//input[@name='verificationCode']")
                time.sleep(random.uniform(0.50, 1.5))
                otp_box.send_keys(opt)
                time.sleep(random.uniform(0.50, 1.5))
                driver.find_element_by_xpath(".//input[@id='ch-auth-app-submit']").click()

                time.sleep(random.uniform(0.50, 1.5))
                driver.find_element_by_xpath(".//input[@id='enable-mfa-form-submit']").click()

                print(json.dumps([{'Key': 'End', 'Value': '2FA has been set up for amazon.'}]))
                end = input()

            os.remove("./barcode.png")
            return 0
    except Exception as e:
        print(json.dumps([{'Key': 'End', 'Value': str(e)}]))
        end = input()
        return 1


'''
    Util function for converting HTML canvas to png.
'''


def canvas_to_png(element):
    canvas_base64 = driver.execute_script("return arguments[0].toDataURL('image/png').substring(21);", element)
    png_image = base64.b64decode(canvas_base64)
    with open("./bar_resize.png", "wb") as f:
        f.write(png_image)

    im = Image.open("./bar_resize.png")

    im_resize = im.resize((128, 128))
    im_resize.save("./bar_resized.png", "PNG")

    with open("./bar_resized.png", "rb") as f:
        png_image = f.read()

    im.close()
    os.remove("./bar_resized.png")
    os.remove("./bar_resize.png")

    return png_image


def send_keys(element, keys, lower_bound=0.05, upper_bound=.30):
    for key in keys:
        element.send_keys(key)
        time.sleep(random.uniform(lower_bound, upper_bound))


# def test(username, password, phone_number, auth_method):
#     #sys.stdin.flush()
#     print(json.dumps([{"Key": "QrImage", "Value": json.dumps({"Image": base64.b64encode("ASCII".encode('ascii')).decode('ascii'), "TextCode": "fdsfsdf", "Label": "Enter CAPTCHA"})}]))
#     #print(json.dumps([{"Key": "TextData", "Value": "Apples"}]))
#     line = input()
#     print(json.dumps([{"Key":"TextData","Value":line}]))

def test(username, password, phone_number, auth_method):
    with open("./barcode.png", "rb") as f:
        qr_img = f.read()
        time.sleep(random.uniform(0.50, 1.5))
        secret_code = "Apples jkfdsklfjklsdjfklsdjfklsdjklfjsdklfjsdkljfklsdjfklsdjfklsdjfklsdjfklsjdklfjslk"
        my_json = json.dumps([{'Key': 'QrImage', 'Value': json.dumps(
            {'Image': base64.b64encode(qr_img).decode('ascii'), 'TextCode': secret_code,
             "Label": "Enter code from 2FA app."})}])
        print(my_json)
        opt = input()

#css=#enable-tfa-verify-email .c-btn-primary
#xpath=(//button[@type='button'])[13]
#xpath=//div[@id='enable-tfa-verify-email']/div[2]/div[2]/button[2]
# def twitter(username, password, phone_number, auth_type):
#         driver.get('https://twitter.com/login')
#         username_box = driver.find_element_by_name('session[username_or_email]')
#         username_box.send_keys(username)
#         password_box = driver.find_element_by_name('session[password]')
#         password_box.send_keys(password)
#         driver.find_element_by_xpath("//button[contains(text(), 'Log in')]").submit()

#reddit("Apples", "Apples", "123-456-7981", "sms")

#reddit('authenticationtest', 'O6By#3mqr%ChdYpG3jH4', '916-293-2234', 'sms')

#pinterest('jonathan@isrl.byu.edu', '20TV6Y73ksaA2x03$#Wo', '916-293-2234', 'sms')


#table_to_png(None)
#github('jonathan@isrl.byu.edu', 'FGJy$QvohSsP5z0eJ&Do', '916-293-2234', 'app')

# facebook('hkxdfyk_baostein_1571326519@tfbnw.net', 'jmw9y9xo8n1', '916-293-2234', 'app')

# google('byuisrl2fa', 'yTJe22EebCKtmCTMKQTv', '916-293-2234', "sms")

# amazon("byuisrl2fa@gmail.com", 'yTJe22EebCKtmCTMKQTv', '801-901-3349', 'sms')

#test("byuisrl2fa@gmail.com", 'yTJe22EebCKtmCTMKQTv', '916-293-2234', 'sms')

# facebook('cwdtzdd_letuchyberg_1574114973@tfbnw.net', 'q4mgfytihrf', '801-901-3349', 'app')
