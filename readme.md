# Installing Keepass TwoFactor extension

1. Copy **bot.py** and **chromedriver.exe** to the release directory.

	```
	C:\Users\USERNAME\Build\PROJECT_DIR\Release
	```

	In case something is broken a backup release directory has been made called
	release.zip. Copy the contents of the directory in to the PROJECT_DIR/Release
	directory.

2. Open **TwoFactorPlugin.sln** in visual studio.

3. Click **project -> Build Solution**.

4. In 'C:\Users\USERNAME\Build\PROJECT_DIR\Release' **Click keepass.exe**.

5. The plugin is working if you can see **Tools -> Setup Two Factor**

# Project Details

* output - Contains prebuild release DLL file for windows 10.

* TwoFactorExt.cs - Holds logic to add the menu option 'Setup TwoFactor' in the keepass appliction.

* TwoFactorForm.cs - Main logic for the plugin. Code for calling the python script, saving '2FA Enabled' to the database and other logic related to the plugin.

* TwoFactorForm.Designer.cs - Code for the GUI of the windows form for the plugin.

* bot.py - Python script that uses selenuim to enable 2FA authentication on websites.

* chromedriver.exe - Needed for selenuim to run.

# Project Notes

## Running ISRL 2FA Extension for Keepass

* Ensure that TwoFactor.dll is in the release directory. Instructions on how to build the extension dll will come later.
* Enusre that bot.py is in the relesse directory.
* Click KeePass.exe
* If you are using the same database from the vm the password is: `apples`(no quotes)
* On the Keepass menu, click Tools -> Setup TwoFactor.
* This menu will show you all 2FA services that you can setup.
	- A service will appear if:
	   * If it is enabled in TwoFactorForm.cs list titled services_enabled. At the time of writing it consisted of { "google", "pinterest", "reddit", "github", "facebook", "amazon" }
           * It is not already setup. The string "2FA is enabled." does not exist in the notes. Note: deleting 2FA is enabled in the notes does not remove the configuration on the target website. You have to do this by hand.

## General Info

Code required for the keepass extension are coded in C#. This means all code for deciding what buttons to show, and menu items for the extension are coded in C#.

Code for setting up two factor is done in python using selenuim with a chrome driver. Code for this can be found in bot.py in the release folder.

These two process talk to each other while running via stdin, stdout. The selenuim python bot will dump data in json format to stdout while the C# extension will read that data and decided which message prompt to show.
It prompts a user dialog and returns the user input back to the python process via stdin.

A kind of weird bug is the python script has to print(THE DATA) before calling input() with empty parenthesis to work with the C# reading/writing. (as of: 11/14/19)

## The Keepass Extension

Stuff to add the menu display items to the keepass application are found in TwoFactorExt.cs

GUI stuff is found in TwoFactorForm.Designer.cs

Extension logic is found in TwoFactorForm.cs

## The Different Directories

* C# Code C:\Users\Me\Desktop\SamplePlugin-2.20-Source
* Python Code found in ~/Build/Release

* Run the keepass application from ~/Release to run with all the dependecies in the right place.

## Debugging

Websites can change their HTML code breaking part of the python selenuim scrape. The easiest way to find out what is happening is run bot.py seperatly from the extension. This allows you to see the stack trace and the line that is causing the problem.
